import { StatusBar } from 'expo-status-bar';
import React,{useState} from 'react';
import { StyleSheet, Text, View, CheckBox, Button, SafeAreaView, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { SET_LOGIN_STATUS } from '~/globals/actions'
import Container from '../container';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import CustomButton from '../CustomButton';
import ActionButton from 'react-native-action-button';
import { Ionicons } from '@expo/vector-icons';

const SignupForm = () => {
    const navigation = useNavigation();

    const dispatch = useDispatch();
    const [isSelected, setSelection] = useState(false);
    return (

        <>
        <Container>
        <View style={{alignItems:'center',paddingBottom:wp('5%')}}>
                <Text style={{fontWeight:'bold',fontSize:25}}>Little<Text style={{fontWeight:'bold',fontSize:25,color:'#43a7d8'}}>MORE</Text></Text>
            </View>
            <View style={{alignItems:'center',paddingBottom:wp('5%')}}>
               <Text style={{fontSize:16}}>Set up your account.</Text>
            </View>
            <View style={{paddingTop:10,paddingBottom:hp('1%')}}>
                <View style={Style.InputField}>
                    <Text style={Style.InputLable}>Username</Text>
                    <TextInput style={{borderWidth:1,borderRadius:4,padding:7}}/>
                </View>
                <View style={Style.InputField}>
                    <Text style={Style.InputLable}>E-mail</Text>
                    <TextInput style={{borderWidth:1,borderRadius:4,padding:7}}/>
                </View>
                <View style={Style.InputField}>
                    <Text style={Style.InputLable}>Password</Text>
                    <TextInput style={{borderWidth:1,borderRadius:4,padding:7}}/>
                </View>
            </View>
           
             <View style={{flexDirection:'row',justifyContent:"space-between"}}>
                <View style={{flexDirection:'row'}}>
                <View style={Style.checkboxContainer}>
                    <Text style={Style.label}>Are you a registered user?</Text>
                </View>
                </View>
                <Text style={{margin:6,fontWeight:'bold'}} onPress={() => navigation.navigate('Login')}>Login Now</Text>
             </View>
             <View style={{flexDirection:'row',justifyContent:"space-between"}}>
             <CustomButton textStyle={{color:'#fff',letterSpacing:1}} buttonStyle={Style.buttonA} title="Submit"
                onPress={() => dispatch({ type: SET_LOGIN_STATUS, status: true })}/>
             <CustomButton textStyle={{color:'#000',letterSpacing:1}} buttonStyle={Style.buttonB} title="Cancel"
                onPress={() => navigation.navigate('Login')}/>
               
             </View>
             

        </Container>
        <ActionButton 
               buttonColor="#8a92ff"
                />
        </>
    );
}

const Style = StyleSheet.create({
    InputField:{
        paddingTop:10,
    },
    InputLable:{
        paddingBottom:hp('1%')
    },
    buttonA:{
        width:wp('37%')  
    },
    buttonB:{
        width:wp('52%'),
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor:'#000'
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 6,
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin:5,
    },
})

export default SignupForm;