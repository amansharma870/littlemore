import * as React from 'react';
import {SafeAreaView, StyleSheet, ViewStyle} from 'react-native';

const Container = (props) => {
  return (
    <SafeAreaView style={[styles.container, props.containerStyles]}>
      {props.children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems:'stretch',
    flex:1,
    justifyContent:"center",
    marginHorizontal:15
  },
});

export default Container;