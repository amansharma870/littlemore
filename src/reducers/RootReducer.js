import { combineReducers } from 'redux';
import user from './user/UserReducer';

export default combineReducers({
    user
})