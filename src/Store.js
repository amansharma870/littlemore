// redux store
import { createStore } from 'redux';
import reducer from '~/reducers/RootReducer';

const store = createStore(reducer);
console.log(store.getState());

export default store;