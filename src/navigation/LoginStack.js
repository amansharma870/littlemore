import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginView from '~/screens/Login/LoginView'
import SignupView from '~/screens/SignUp/SignupView';
import ForgotPasswordView from '~/screens/Forgot/ForgotPasswordView';

const Stack = createStackNavigator();

const LoginStack = () => {
    return (
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen options={{headerShown:false}} name="Login" component={LoginView} />
            <Stack.Screen name="ForgotPassword" component={ForgotPasswordView} />
            <Stack.Screen name="SignUp" component={SignupView} />
        </Stack.Navigator>
    );
}

export default LoginStack;