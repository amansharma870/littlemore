import React from 'react';
import { View, Text } from 'react-native';
import ForgotPasswordForm from '~/components/Forgot/ForgotPasswordForm';

const ForgotPasswordView = () => {
    return <View>
             <ForgotPasswordForm />
          </View>
}

export default ForgotPasswordView;