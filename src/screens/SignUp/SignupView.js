import React from 'react';
import { View, Text } from 'react-native';
import LoginForm from '~/components/Login/LoginForm';
import SignupForm from '~/components/Signup/SignupForm';

const SignupView = () => {
    return <View style={{flex: 1}}>
        <SignupForm />
    </View>
}

export default SignupView;